import React from 'react';

import swal from 'sweetalert';

import { FaAddressCard } from 'react-icons/fa';
import { formatPrice } from '../../utils/helpers';
import { useDispatch } from 'react-redux';
import { cartActions } from '../../store/cart-slice';
import { PAYMENT } from '../../utils/constants';
import { Navigate, useNavigate } from 'react-router-dom';


const CheckoutContent = ({ totalPrice, name }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const payInvoice = () => {
        dispatch(cartActions.clearCart());
        swal({
            title: "Invoice Paid!",
            text: `You paid ${formatPrice(totalPrice)}`,
            icon: "success",
            button: "OK!",
        }).then(() => {
            navigate("/");
        });

    };

    return (
        <div>
            <div className="w-[95%] sm:w-[80%] lg:w-[50%] mx-auto px-8 py-12 shadow-lg rounded-xl">
                <h2 className="capitalize text-3xl font-bold tracking-wider mb-10 leading-relaxed">Hello, {name}</h2>
                <div className='flex justify-between items-center'>
                    <p className="text-lg capitalize mb-4">Subtotal : </p>
                    <span className="italic font-semibold">{formatPrice(totalPrice)}</span>
                </div>
                <div className='flex justify-between items-center'>
                    <p className="text-lg capitalize mb-4">estimated shipping : </p>
                    <span className="italic font-semibold">{formatPrice(5.99)}</span>
                </div>
                <div className='flex justify-between items-center'>
                    <p className="text-lg capitalize mb-4">shipping discount : </p>
                    <span className="italic font-semibold">{formatPrice(-5.99)}</span>
                </div>
                <hr className='my-6' />
                <div className='flex justify-between items-center'>
                    <p className="text-xl capitalize mb-4 font-bold">Total : </p>
                    <span className="italic font-bold">{formatPrice(totalPrice)}</span>
                </div>
            </div>
            <div className="w-[95%] sm:w-[80%] lg:w-[50%] mx-auto mt-12 px-8 py-12 shadow-lg rounded-xl">
                <div className="w-[80%] m-auto rounded">

                    {
                        PAYMENT.map((i, k) => {
                            return (
                                <div className="flex mt-2 mb-5 items-center pl-4 border border-gray-200 rounded dark:border-gray-700">
                                    <input id={`bordered-radio-${k}`} type="radio" value="" name="payment" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                                    <label for={`bordered-radio-${k}`} className="w-full py-4 ml-2 text-sm font-medium text-black-900 dark:text-black-300">{i}</label>
                                </div>
                            )
                        })
                    }

                    <div className='flex'>
                        <span className="text-3xl flex items-center justify-center rounded-l border border-gray-100 border-r-0 py-1 px-2 bg-gray-300  text-black">
                            <FaAddressCard />
                        </span>
                        <input type="text" placeholder="Jl raya no 1" className="form-input w-full rounded-r" required />
                    </div>
                </div>
                <button className="mt-4 capitalize py-3 text-xl font-bold tracking-widest text-primary bg-secondary-100 w-full drop-shadow-2xl" onClick={payInvoice}>Pay</button>
            </div>
        </div>
    );
};


export default CheckoutContent;