import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";


const LoginRedirect = () => {
    const { isAdmin, user } = useSelector((state) => state.auth);

    return (
        isAdmin ? <Navigate to='/' replace /> : user ? <Navigate to='/products' replace /> : <Outlet />
    );

};


export default LoginRedirect;