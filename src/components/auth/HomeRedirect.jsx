import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";


const LoginRedirect = () => {
    const { isAdmin, isAuthenticated } = useSelector((state) => state.auth);

    return (
        !isAuthenticated ?  <Navigate to='/login' replace /> : <Outlet />
    );

};


export default LoginRedirect;