// export const HERO_URL = 'https://tech-ecommerce.s3.eu-west-2.amazonaws.com/images/hero2.png';
export const HERO_URL = 'https://www.pngmart.com/files/15/Vector-Fashion-Woman-PNG-Photos.png';

export const ABOUT_IMG_URL = 'https://tech-ecommerce.s3.eu-west-2.amazonaws.com/images/hero1.png';

export const API_URL = 'https://skripsi.elcodee.com';


export const CATEGORIES = [
    'all', 'tshirt', 'jeans', 'dress', 'monitor'
];

export const BRANDS = [
    'All', 'H&M', 'LV', 'PULL & BEAR', 'UNIQLO'
];

export const PAYMENT = [
    'BANK', 'E-WALLET', 'MINI MARKET', 'COD'
];